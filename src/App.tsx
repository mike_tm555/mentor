import React from 'react';
import Pages from "./pages";
import { mxCreate } from "mx-store"
import countries from './models/countries';
import employees from './models/employees';
import users from './models/users';
import './App.scss';

mxCreate({
    countries,
    employees,
    users,
});

function App() {
  return (
    <div className="App light-theme">
      <Pages/>
        <div id="drag-area"/>
    </div>
  );
}

export default App;
