import React from "react";
import { Trans } from 'react-i18next';
import {PageProps} from "../../types/typings";
import Page from "../../components/page";
import './index.scss';
import {Font} from "../../components/typography/font";
import Button from "../../components/typography/button";

function HomePage(props: PageProps) {
    const title = <Trans>{"mentor.app"}</Trans>;
    return (
        <Page title={title}
              pageClass="home-page">
            <div className="home-page-demo">
                <Font type="h1" theme="dark" className="home-demo-desc"><Trans>{"mentoring.easy"}</Trans></Font>
                <Font type="h2" theme="dark" className="home-demo-desc"><Trans>{"mentoring.description"}</Trans></Font>
                <Button size="large" link="/register"><Trans>{"request.demo"}</Trans></Button>
            </div>
        </Page>
    )
}

export default HomePage;
