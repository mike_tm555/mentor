import React from "react";
import {
    BrowserRouter,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import { mxConnect } from 'mx-store';
import HomePage from "./home";
import RegisterPage from "./register";
import ProfilePage from "./profile";

type PagesProps = {
    user: any,
}

function Pages(props: PagesProps) {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    {props.user?.token ? <Redirect to="/profile/"/> : <HomePage />}
                </Route>
                <Route exact path="/register/">
                    {props.user?.token ? <Redirect to="/profile/"/> : <RegisterPage />}
                </Route>
                <Route exact path="/profile/">
                    {!props.user?.token ? <Redirect to="/register/"/> : <ProfilePage />}
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

const mapStateToProps = (state: any) => {
    return {
        user: state.users.read('user')
    }
};

export default mxConnect(Pages)(mapStateToProps);
