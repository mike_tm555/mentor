import React from "react";
import { Trans } from 'react-i18next';
import {PageProps} from "../../types/typings";
import Page from "../../components/page";
import Registration from "../../components/registration";
import './index.scss';

function RegisterPage(props: PageProps) {
    const title = <Trans>{"mentor.registration"}</Trans>;
    return (
        <Page title={title}
              pageClass="registration-page">
            <div className="registration-form">
                <Registration/>
            </div>
        </Page>
    )
}

export default RegisterPage;
