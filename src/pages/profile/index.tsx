import React from "react";
import './index.scss';
import ProfileForm from '../../components/profile'
import {PageProps} from "../../types/typings";
import {Trans} from "react-i18next";
import Page from "../../components/page";

function ProfilePage(props: PageProps) {
    const title = <Trans>{"mentor.profile"}</Trans>;
    return (
        <Page title={title}
              pageClass="profile-page">
            <ProfileForm/>
        </Page>
    )
}

export default ProfilePage;
