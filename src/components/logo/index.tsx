import React from "react";
import { Link } from 'react-router-dom';
import './index.scss';

interface LogoProps {
    src?: string,
    alt?: string
}

function Logo(props: LogoProps) {
    return (
        <Link to="/" className="app-logo">
            <img src={props.src || `/assets/images/logo.png`} alt={props.alt}/>
        </Link>
    )
}

export default Logo;
