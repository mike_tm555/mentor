import React from 'react';
import Logo from "../../logo";
import './index.scss';
import {Font} from "../../typography/font";

interface PageHeaderProps {
    title: any,
    content: any
}

function PageHeader(props: PageHeaderProps) {
    return (
        <div className="page-header">
            <Logo/>
            {props.title && (
                <div className="page-header-title">
                    <Font type="h1">{props.title}</Font>
                </div>
            )}
            {props.content && (
                <div className="page-header-content">
                    {props.content}
                </div>
            )}
        </div>
    )
}

export default PageHeader;
