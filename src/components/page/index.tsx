import React from 'react';
import PageHeader from "./header";
import PageBody from "./body";
import './index.scss';

export interface PageProps {
    header?: any,
    title?: any,
    children: any,
    pageClass?: string
}

function Page(props: PageProps) {
    return (
        <div className="app-page">
            <div className="app-page-header">
                <PageHeader title={props.title} content={props.header}/>
            </div>
            <div className="app-page-body">
                <PageBody pageClass={props.pageClass}>
                    {props.children}
                </PageBody>
            </div>
        </div>
    )
}

export default Page;
