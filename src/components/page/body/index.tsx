import React from 'react';
import './index.scss';

export interface PageBodyProps {
    children: any,
    pageClass?: string
}

function PageBody(props: PageBodyProps) {
    let pageClass = `page-body`;

    if (props.pageClass) {
        pageClass += ` ${props.pageClass}`;
    }

    return (
        <div className={pageClass}>
            {props.children}
        </div>
    )
}

export default PageBody;
