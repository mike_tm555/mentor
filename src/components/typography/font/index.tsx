import React from "react";
import './index.scss';

type FontProps = {
    type: keyof JSX.IntrinsicElements,
    children: any,
    className?: string,
    theme?: string,
} & React.HTMLAttributes<HTMLOrSVGElement>

export function Font(props: FontProps) {
    const { type: FontTag, children} = props;

    let className = `app-font`;

    if (props.className) {
        className += `  ${props.className}`;
    }

    className += ` ${props.theme || 'light'}`;

    return (
        <FontTag className={className}>
            {children}
        </FontTag>
    )
}
