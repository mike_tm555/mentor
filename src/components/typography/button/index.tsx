import React from "react";
import { Link } from 'react-router-dom';
import './index.scss';

type ButtonProps = {
    design?: string,
    type?: string,
    size?: string,
    className?: string,
    children: any,
    icon?: any,
    link?: string,
    action?: () => void
}

function Button(props: ButtonProps) {
    let className = `app-button  ${props.className ? props.className : ""}`;

    className += ` ${props.design || "simple"}`;
    className += ` ${props.size || "medium"}`;
    className += ` ${props.link ? "link-button" : ""}`;

    return props.link ? (
        <Link className={className} to={props.link}>
            {props.icon && (<span className="button-icon">{props.icon}</span>)}
            {props.children}
        </Link>
    ) : (
        <button className={className}>
            {props.icon && (<span className="button-icon">{props.icon}</span>)}
            {props.children}
        </button>
    )
}

export default Button;
