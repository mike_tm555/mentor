import React, {useEffect, useMemo, useState} from "react";
import { Trans } from 'react-i18next';
import { mxConnect } from 'mx-store';
import './index.scss';
import Form from "../../components/form";
import Validate from "../../components/form/validate";
import {
    firstName,
    lastName,
    email,
    country,
    company,
    phone
} from "../constants/fields";
import Group from "../form/fields/group";

const PROFILE_FORM = {
    fields: {
        firstName,
        lastName,
        email,
        country,
        company,
        phone
    } as any,
    button: (<Trans>{"registration.submit"}</Trans>)
};

type RegistrationProps = {
    countries: any,
    employees: any,
    user: any,
    saveUser: any,
};

function ProfileForm(props: RegistrationProps) {
    const [user, setUser] = useState(props.user || {});
    const [form, setForm] = useState(PROFILE_FORM) as any;
    const [formTouched, setFormTouched] = useState(false);

    useEffect(() => {
        if (!formTouched  && user) {
            const newForm = {...form};

            Object.keys(newForm.fields).forEach((key: string) => {
                newForm.fields[key] = {
                    ...newForm.fields[key],
                    value: user[key] || newForm.value
                };
            });

            setForm(newForm);
            setFormTouched(true);
        }
    }, [user, form, setForm, formTouched, setFormTouched]);

    useMemo(() => {
        const newForm = {...form};

        if (props.countries.length)
            if (!newForm.fields.country.data.options.length) {
                newForm.fields.country.data.options = props.countries.map((country: any) => {
                    return {
                        value: country.code,
                        label: country.name
                    }
                });
                setForm(newForm);
            }
    }, [props.countries, form, setForm]);

    const changeForm = (value: any, name: string) => {
        const newForm = {...form};
        newForm.fields[name].value = value;
        setForm(newForm);
    };

    const submitForm = (data: any) => {
        const newForm = {...form};

        newForm.fields = Validate(data);

        let stepHasError = false;

        for (let key in newForm.fields) {
            const field = newForm.fields[key];
            if (field.error) {
                stepHasError = true;
                break;
            }
        }

        if (!stepHasError) {
            const newUser = {...user};

            Object.keys(newForm.fields).forEach((key: string) => {
                const field = newForm.fields[key];
                newUser[key] = field.value;
            });

            props.saveUser('user', newUser);
            setUser(newUser);
        }

        setForm(newForm);
    };

    const changeGroup = (items: any[], name: string) => {
        const newUser = {...user};
        newUser[name] = items;
        props.saveUser('user', newUser);
        setUser(newUser);
    };

    return (
        <div className="user-profile">
            <div className="profile-form">
                <Form fields={form.fields}
                      change={changeForm}
                      submit={(data: any) => {
                          submitForm(data);
                      }}
                      button={form.button}
                />
            </div>
            {props.employees.length && user && (
                <Group value={user.group}
                       data={{options: props.employees}}
                       name="group"
                       onChange={changeGroup}/>
            )}
        </div>
    )
}

const mapStateToProps = (state: any) => {
    return {
        countries: state.countries.getAll(),
        employees: state.employees.getAll(),
        saveUser: state.users.save,
        user: state.users.read('user')
    }
};

export default React.memo(mxConnect(ProfileForm)(mapStateToProps));
