import React from "react";
import {Trans} from "react-i18next";

export const firstName = {
    label: <Trans>{'field.firstName'}</Trans>,
    component: 'Input',
    name: 'firstName',
    data: {
        type: 'name'
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        }
    }
};

export const lastName = {
    label: <Trans>{'field.lastName'}</Trans>,
    component: 'Input',
    name: 'lastName',
    data: {
        type: 'name'
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        }
    }
};

export const email = {
    label: <Trans>{'field.email'}</Trans>,
    component: 'Input',
    name: 'email',
    data: {
        type: 'email'
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        },
        email: {
            data: null,
            message: <Trans>{'field.invalid'}</Trans>
        }
    }
};

export const country = {
    label: <Trans>{'field.country'}</Trans>,
    component: 'Select',
    name: 'country',
    data: {
        options: [],
        default: {
            value: null,
            label: "Select Country"
        }
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        }
    }
};

export const  company = {
    label: <Trans>{'field.company'}</Trans>,
    component: 'Input',
    name: 'company',
    data: {
        type: 'string'
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        }
    }
};

export const phone = {
    label: <Trans>{'field.phone'}</Trans>,
    component: 'Input',
    placeholder: '+374 77878349',
    name: 'phone',
    data: {
        type: 'tel'
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        },
        phone: {
            data: null,
            message: <Trans>{'field.invalid'}</Trans>
        }
    }
};

export const group = {
    component: 'Group',
    name: 'group',
    data: {
        options: []
    },
    rules: {
        required: {
            data: null,
            message: <Trans>{'field.required'}</Trans>
        }
    }
};
