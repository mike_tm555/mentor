import React from "react";
import Field from "./fields/index";
import Button from "../typography/button";

type FormProps = {
    fields: any,
    change: (value: any, name: string) => void,
    submit: (data: any) => void,
    button: any
}

function Form(props: FormProps) {
    const submit = (e: any) => {
        e.preventDefault();

        if (typeof props.submit === 'function') {
            props.submit(props.fields);
        }
    };

    return (
        <div className='app-form'>
            <form onSubmit={submit}>
                {Object.keys(props.fields).map((key: string, index: number) => {
                    const field = props.fields[key];
                    return (
                        <Field
                            key={key + index}
                            onChange={props.change}
                            {...field}/>
                    )
                })}
                <Button size="medium">
                    {props.button}
                </Button>
            </form>
        </div>
    )
}

export default Form;
