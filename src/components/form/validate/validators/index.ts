export { default as alpha } from './alpha';
export { default as email } from './email';
export { default as required } from './required';
export { default as phone } from './phone';
