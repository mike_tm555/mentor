export default function validatePhone(value: string, rule: any) {
    if (value) {
        var found = value.search(/^(\+{1}\d{2,3}\s?[(]{1}\d{1,3}[)]{1}\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}$/);
        return (found > -1);
    }

    return false;
}
