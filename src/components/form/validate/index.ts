import * as Validators from "./validators/index";

const validators = Validators as any;

export default function Validate(fields: any) {
    Object.keys(fields).forEach((key: string) => {
        const field = fields[key];
        Object.keys(field.rules).forEach((key: string) => {
            const validationResult = validators[key](field.value, field.rules[key].data);

            if (!validationResult) {
                field.error = {
                    rule: field.rules[key],
                    message: field.rules[key].message
                }
            } else {
                field.error = undefined;
            }
        });
    });

    return fields;
}
