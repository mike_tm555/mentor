import React, {useState} from "react";
import {Trans} from "react-i18next";
import DataTable from "../../data-table";
import "./group.scss";

type GroupProps = {
    data?: any,
    value: any,
    name: string,
    inputClass?: string,
    onInput?: (value: any, name: string) => void
    onChange: (value: any, name: string) => void
}

const MAX_ITEMS_COUNT = 5;

function Group(props: GroupProps) {
    const [groupItems, setGroupItems] = useState(props.value || []);

    const onAddNewItem = (row: any) => {
        if (groupItems.length + 1 <= MAX_ITEMS_COUNT) {
            const newGroupItems = groupItems.concat(row);

            if (typeof props.onChange === 'function') {
                props.onChange(newGroupItems, props.name);
            }

            setGroupItems(newGroupItems);
        }
    };

    const onDeleteGroupItem = (row: any) => {
        const newGroupItems = groupItems.filter((item: any) => {
            return (item !== row)
        });

        if (typeof props.onChange === 'function') {
            props.onChange(newGroupItems, props.name);
        }

        setGroupItems(newGroupItems);
    };

    const onChange = (rows: any[]) => {
        setGroupItems(rows);
        if (typeof props.onChange === 'function') {
            props.onChange(rows, props.name);
        }
    };

    let labelsMap: any[] = [];
    if (props.data.options.length) {
        Object.keys(props.data.options[0]).forEach((optKey: string) => {
            labelsMap.push({
                key: optKey,
                name: optKey,
            })
        });
    }

    const groupItemsLabels = [{
        key: 'delete',
        type: 'action',
        icon: <i className="fa fa-trash"/>,
        action: ((row: any) => {
            onDeleteGroupItem(row);
        })
    }].concat(labelsMap);

    const employeesLabels = [{
        key: 'add',
        type: 'action',
        icon: <i className="fa fa-plus"/>,
        action: ((row: any) => {
            onAddNewItem(row);
        })
    }].concat(labelsMap);

    const employeesList = props.data.options.filter((employee: any) => {
        return (groupItems.indexOf(employee) <= -1);
    });

    return (
        <div className="manage-group">
            <div className="user-group">
                <DataTable
                    reorder={true}
                    empty={<Trans>{"empty.rows"}</Trans>}
                    labels={groupItemsLabels}
                    onChange={onChange}
                    rows={groupItems}/>
            </div>
            <div className="employees-list">
                <DataTable
                    reorder={false}
                    empty={<Trans>{"empty.rows"}</Trans>}
                    labels={employeesLabels}
                    rows={employeesList}/>
            </div>
        </div>
    );
}

export default Group;
