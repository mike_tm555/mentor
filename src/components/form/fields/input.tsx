import React from "react";

type InputProps = {
    data?: any,
    value: any,
    name: string,
    placeholder?: string,
    inputClass: string,
    onInput: (value: any, name: string) => void
    onChange: (value: any, name: string) => void
};

function Input(props: InputProps) {
    const onInput = (e: any) => {
        const inputValue = e.target.value;
        if (typeof props.onInput === 'function') {
            props.onInput(inputValue, props.data.name);
        }
    };

    const onChange = (e: any) => {
        const inputValue = e.target.value;
        if (typeof props.onChange === 'function') {
            props.onChange(inputValue, props.name);
        }
    };

    return (
        <input
            id={props.data.name}
            className={props.inputClass}
            type={props.data.type}
            name={props.data.name}
            placeholder={props.placeholder}
            onChange={onChange}
            onInput={onInput}
            value={props.value}/>
    )
}

export default Input;
