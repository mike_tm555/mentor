import React from 'react';
import Input from "./input";
import Select from "./select";
import Group from "./group";
import './index.scss';

const Fields = {
    Input,
    Select,
    Group,
} as any;

type FieldProps = {
    label: any,
    name: string,
    placeholder?: string,
    size?: string,
    className?: string,
    component: string,
    onChange?: (value: any, name: string) => void
    onInput?: (value: any, name: string) => void,
    data: any,
    value: any,
    error?: any,
};

function Field(props: FieldProps) {
    const { component, label, data, name, value, onChange, onInput} = props;
    let className = `form-field`;

    className += ` ${props.size || "medium"}`;

    if (props.className) {
        className += ` ${props.className}`;
    }

    let inputClassName = ` form-input`;

    if (props.error) {
        inputClassName += ` error`
    }

    const Component = Fields[component];

    const fieldComponent = (
        <Component
            inputClass={inputClassName}
            data={data}
            name={name}
            placeholder={props.placeholder || ''}
            onChange={onChange}
            onInput={onInput}
            value={value || ''}/>
    );

    return (
        <div className={className}>
            {label ? (
                <React.Fragment>
                    <label className="form-field-label"
                           htmlFor={name}>
                        {label}
                    </label>
                    <div className="form-filed-input">
                        {fieldComponent}
                    </div>
                </React.Fragment>
            ) : (fieldComponent)}

            {props.error && (
                <span className="error-message">{props.error.message}</span>
            )}
        </div>
    )
}

export default Field;
