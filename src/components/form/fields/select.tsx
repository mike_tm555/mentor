import React, {useState} from "react";

type InputProps = {
    data?: any,
    value: any,
    name: string,
    inputClass: string,
    onInput: (value: any, name: string) => void
    onChange: (value: any, name: string) => void
};

function Select(props: InputProps) {
    const [value, setValue] = useState(props.value);

    const onChange = (e: any) => {
        const inputValue = e.target.value;
        if (typeof props.onChange === 'function') {
            props.onChange(inputValue, props.name);
        }
        setValue(inputValue);
    };

    return (
        <select name={props.name}
                onChange={onChange}
                value={value}
                className={props.inputClass}>
            {props.data.default && (
                <option value={props.data.default.value}>
                    {props.data.default.label}
                </option>
            )}
            {props.data.options.map((opt: any, index: number) => {
                return (
                    <option key={opt.value + index}
                            value={opt.value}>
                        {opt.label}
                    </option>
                )
            })}
        </select>
    )
}

export default Select;
