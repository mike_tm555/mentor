import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";

function Draggable(props: any) {
    const dragArea = document.getElementById('drag-area');

    if (dragArea) {
        const style = {
            left: props.pos.x + 'px',
            top: props.pos.y + 'px',
        };
        return ReactDOM.createPortal((
            <div className="draggable" style={style}/>
        ), dragArea);
    }

    return null;
}

export default Draggable;
