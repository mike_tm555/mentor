import React, {useEffect, useState} from "react";
import Draggable from "../draggable";
import './index.scss';

type TableProps = {
    empty: any,
    labels: any[],
    rows: any[],
    onSelect?: (row: any) => void,
    onDelete?: (row: any) => void,
    onChange?: (rows: any[]) => void,
    reorder?: boolean,
}

function DataTable(props: TableProps) {
    const [dragPos, setDragPos] = useState(null) as any;
    const [dragIndex, setDragIndex] = useState(null) as any;
    const [dropIndex, setDropIndex] = useState(null) as any;
    const [rows, setRows] = useState(props.rows);

    useEffect(() => {
        if (props.rows.length) {
            setRows(props.rows);
        }
    }, [props.rows]);

    const handleMouseMove = (e: any) => {
        setDragPos({
            x: e.clientX,
            y: e.clientY,
        });
    };

    const  activateDropZone = (index: number | null) => {
        setDropIndex(index)
    };

    const handleMouseUp = (e: any) => {
        setDragPos(null);
        window.removeEventListener('mousemove', handleMouseMove);
        window.removeEventListener('mouseup', handleMouseUp);
    };

    const handleReorder = (dragIndex: number, dropIndex: number) => {
        let newRows = [...rows];

        const item = newRows.splice(dragIndex, 1);
        newRows.splice(dropIndex, 0, item[0]);
        setRows(newRows);

        if (typeof props.onChange === 'function') {
            props.onChange(newRows);
        }

        setDragIndex(null);
        setDropIndex(null);
        setDragPos(null);
    };

    const handleMouseDown = (e: any, index: number) => {
        if (!props.reorder) {
            return;
        }
        setDragPos(null);
        setDragIndex(index);
        window.addEventListener('mousemove', handleMouseMove);
        window.addEventListener('mouseup', handleMouseUp);
    };

    return (
        <div className="data-table">
            {dragPos && <Draggable pos={dragPos}/>}
            <div className="table-header">
                {props.labels.map((label: any) => {
                    if (label.type === 'action') {
                        return (
                            <div key={label.key}
                                 className="header-label action-label">
                                {label.icon}
                            </div>
                        )
                    }

                    return (
                        <div key={label.key}
                             className="header-label">
                            {label.name}
                        </div>
                    )
                })}
            </div>
            {rows.length ?
                (<div className="table-body">
                        {rows.map((row: any, index: number) => {
                            const random = Math.random();
                            let className = ` table-row`;
                            if (index === dragIndex) {
                                className += ` hidden`;
                            }

                            return (
                                <div key={random + index}
                                     onMouseDown={(e) => {
                                         handleMouseDown(e, index);
                                     }}
                                     data-index={index}
                                     data-droppable={true}
                                     className={className}>
                                    {(props.reorder && dragIndex !== null) && (
                                        <div className={`drop-zone top ${dropIndex === index ? "active" : ""}`}
                                             onMouseEnter={(e) => {
                                                 activateDropZone(index);
                                             }}
                                             onMouseUp={() => {
                                                 handleReorder(dragIndex, dropIndex);
                                             }}
                                             onMouseOut={() => {
                                                 activateDropZone(null);
                                             }}/>
                                    )}
                                    {(props.reorder && dragIndex !== null) && (
                                        <div className={`drop-zone bottom ${dropIndex === index + 1 ? "active" : ""}`}
                                             onMouseEnter={(e) => {
                                                 activateDropZone(index + 1);
                                             }}
                                             onMouseUp={() => {
                                                 handleReorder(dragIndex, dropIndex);
                                             }}
                                             onMouseOut={() => {
                                                 activateDropZone(null);
                                             }}/>
                                    )}
                                    {props.labels.map((label: any, key: number) => {
                                        if (label.type === 'action') {
                                            return (
                                                <div key={label.key + index}
                                                     className="row-col action-col"
                                                     onMouseDown={(e) => {
                                                         e.stopPropagation();
                                                         label.action(row);
                                                     }}>
                                                    {label.icon}
                                                </div>
                                            )
                                        }
                                        return (
                                            <div key={label.key + index}
                                                 className="row-col">
                                                {row[label.key]}
                                            </div>
                                        )

                                    })}
                                </div>
                            )
                        })}
                    </div>
                ) : (
                    <div className="table-body">
                        <div className="empty-rows">{props.empty}</div>
                    </div>
                )}
        </div>
    )
}

export default React.memo(DataTable);
