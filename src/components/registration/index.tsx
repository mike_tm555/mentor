import React, {useMemo, useState} from "react";
import { Trans } from 'react-i18next';
import { mxConnect } from 'mx-store';
import './index.scss';
import Form from "../../components/form";
import Validate from "../../components/form/validate";
import {
    firstName,
    lastName,
    email,
    country,
    company,
    phone,
    group
} from "../constants/fields";

const REGISTRATION_FORM = {
    initial: {
        active: true,
        fields: {
            firstName,
            lastName,
            email
        },
        button: (<Trans>{"registration.next"}</Trans>)
    },
    contact: {
        active: false,
        fields: {
            country,
            company,
            phone
        },
        button: (<Trans>{"registration.next"}</Trans>)
    },
    group: {
        active: false,
        fields: {
            group
        },
        button: (<Trans>{"registration.submit"}</Trans>)
    }
};

type RegistrationProps = {
    countries: any,
    employees: any,
    user: any,
    saveUser: any,
};

function Registration(props: RegistrationProps) {
    const [form, setForm] = useState(REGISTRATION_FORM) as any;
    const [user, setUser] = useState(props.user || {});
    const [formTouched, setFormTouched] = useState(false);

    useMemo(() => {
        const newForm = {...form};

        if (props.countries.length)
            if (!newForm.contact.fields.country.data.options.length) {
                newForm.contact.fields.country.data.options = props.countries.map((country: any) => {
                    return {
                        value: country.code,
                        label: country.name
                    }
                });
                setForm(newForm);
            }
    }, [props.countries, form, setForm]);

    useMemo(() => {
        const newForm = {...form};

        if (props.employees.length) {
            if (!newForm.group.fields.group.data.options.length) {
                newForm.group.fields.group.data.options = props.employees;
                setForm(newForm);
            }
        }
    }, [props.employees, form, setForm]);

    useMemo(() => {
        if (Object.keys(user).length > 0 && !formTouched) {
            const newForm = {...form};

            for (let step in newForm) {
                newForm[step].active = false;

                for (let key in newForm[step].fields) {
                    if (!user[key]) {
                        newForm[step].active = true;
                    }
                }

                if (newForm[step].active === true) {
                    break;
                }
            }

            Object.keys(newForm).forEach((step: string) => {
                for (let key in newForm[step].fields) {
                    if (user[key]) {
                        newForm[step].fields[key].value = user[key];
                    }
                }
            });

            setForm(newForm);
            setFormTouched(true);
        }
    }, [user, form, setForm, formTouched, setFormTouched]);

    const changeForm = (value: any, name: string) => {
        const newForm = {...form};

        Object.keys(newForm).forEach((key: string) => {
            if (newForm[key].fields[name]) {
                newForm[key].fields[name].value = value;
            }
        });

        setForm(newForm);
    };

    const submitForm = (data: any, step: string) => {
        const newForm = {...form};

        newForm[step].fields = Validate(data);

        let stepHasError = false;

        for (let key in newForm[step].fields) {
            const field = newForm[step].fields[key];
            if (field.error) {
                stepHasError = true;
                break;
            }
        }

        if (!stepHasError) {
            const newUser = {...user};

            const formKeys = Object.keys(newForm);
            formKeys.forEach((key: string) => {
                newForm[key].active = false;
            });

            const stepIndex = formKeys.indexOf(step);

            let nextStepIndex = stepIndex + 1;

            if (nextStepIndex > formKeys.length - 1) {
                nextStepIndex = stepIndex;
                newUser.token = new Date().getTime();
            }

            newForm[formKeys[nextStepIndex]].active = true;

            Object.keys(newForm[step].fields).forEach((key: string) => {
                const field = newForm[step].fields[key];
                newUser[key] = field.value;
            });

            props.saveUser('user', newUser);

            if (newUser.token) {
                window.location.reload();
            }

            setUser(newUser);
        }

        setForm(newForm);
    };

    const formKeys = Object.keys(form);
    return (
        <div className="registration-steps">
            {formKeys.map((key: string, index: number) => {
                const step = form[key];
                if (step.active === true) {
                    return (
                        <div key={key}
                             className={`registration-step ${key}`}>
                            <Form fields={step.fields}
                                  change={changeForm}
                                  submit={(data: any) => {
                                      submitForm(data, key);
                                  }}
                                  button={step.button}
                            />
                        </div>
                    )
                }

                return null;
            })}
        </div>
    )
}

const mapStateToProps = (state: any) => {
    return {
        countries: state.countries.getAll(),
        employees: state.employees.getAll(),
        saveUser: state.users.save,
        user: state.users.read('user')
    }
};

export default React.memo(mxConnect(Registration)(mapStateToProps));
