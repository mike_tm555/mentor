import { mxStore } from "mx-store";

export default mxStore.register({
    host: 'http://localhost:3000',
    name: 'countries',
    path: '/data/countries.json',
})
